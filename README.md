# taotaoShop [传智播客---淘淘商城项目]

### Dao 层：整合mybatis和spring
#### 需要的jar包：
* 1.mybatis的jar包
* 2.mysql的数据库驱动
* 3.数据库连接池
* 4.mybatis和spring整合包
* 5.spring的jar包
#### 配置文件
* 1.mybatis的配置文件：`SqlMapConfig.xml`
* 2.Spring的配置文件：`applicationContext-dao.xml`
    + 1.数据源
    + 2.数据库连接池
    + 3.配置SqlSessionFactory(mybatis和spring整合中的)
    + 4.配置mapper文件扫描器。

### Service层:
#### 使用的jar包：
* spring的jar包
#### 配置文件：
* applicationContext-service.xml
#### 配置一个包扫描器 
* 扫描所有带@Service注解的类。
#### 事务配置
* applicationContext-trans.xml
    + 1.配置一个事务管理器
    + 2.配置tx
    + 3.配置切面


### 表现层：
#### 使用的jar包
* 使用springmvc，需要使用springmvc和spring的jar包
#### 配置文件：springmvc.xml
* 1.配置注解驱动 （可以替代配置处理器和适配器）
* 2.配置一个视图解析器
* 3.包扫描器，@Controller注解
#### web.xml
* 1.配置springmvc的前端控制器
* 2.spring容器初始化的listener

### 测试整合框架
##### 根据商品id查询商品信息，返回json数据 （jackson包）
* dao层
  + 查询的表 tb_item，根据商品id。可以使用逆向工程生成的代码。
* service层
  + 接收商品id，调用mapper查询商品信息。返回商品的pojo
  + 参数：Long itemId
  + 返回值：TbItem
* controller层
  + 接受商品id,调用Service返回一个商品的pojo,直接响应pojo。需要返回json数据，使用@ResponseBody
    注意：使用@ResponseBody的时候一定要把Jackson包添加到工程中。
  + Url: /item/{itemId}
  + 响应：TbItem
* 解决mapper映射文件不拷贝的问题
  + 需要修改taotao-manager-dao工程的pom文件


### 课程计划
* 商品列表查询工程
    + a.EasyUI
    + b.分页处理
* 商品的添加
    + 商品类目选择EasyUI异步 tree控件的使用
    + 图片上传（图片服务器开头，nginx）
    + 富文本编辑器的使用
    + 添加的实现
* 展示首页
    + 创建一个controller,
