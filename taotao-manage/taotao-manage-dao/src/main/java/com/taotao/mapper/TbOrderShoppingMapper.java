package com.taotao.mapper;

import com.taotao.pojo.TbOrderShopping;
import com.taotao.pojo.TbOrderShoppingExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbOrderShoppingMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int countByExample(TbOrderShoppingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int deleteByExample(TbOrderShoppingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int deleteByPrimaryKey(String orderId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int insert(TbOrderShopping record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int insertSelective(TbOrderShopping record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    List<TbOrderShopping> selectByExample(TbOrderShoppingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    TbOrderShopping selectByPrimaryKey(String orderId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int updateByExampleSelective(@Param("record") TbOrderShopping record, @Param("example") TbOrderShoppingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int updateByExample(@Param("record") TbOrderShopping record, @Param("example") TbOrderShoppingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int updateByPrimaryKeySelective(TbOrderShopping record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_order_shopping
     *
     * @mbggenerated Sun Jan 14 14:24:14 CST 2018
     */
    int updateByPrimaryKey(TbOrderShopping record);
}